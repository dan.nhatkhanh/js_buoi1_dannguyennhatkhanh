/**
 * Input
 * chieuDai
 * chieuRong
 *
 * Todo
 * chuVi = (chieuDai + chieuRong) * 2
 * dienTich = chieuDai * chieuRong
 *
 * Output
 * chuVi
 * dienTich
 *
 */

var chieuDai = 3;
var chieuRong = 5;

var chuVi = (chieuDai + chieuRong) * 2;
console.log("chuVi: ", chuVi);
var dienTich = chieuDai * chieuRong;
console.log("dienTich: ", dienTich);
