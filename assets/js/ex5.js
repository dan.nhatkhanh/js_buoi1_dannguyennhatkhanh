/**
 * Input
 * soHang = 13
 *
 * Todo
 * soDonvi = soHang % 10
 * soChuc = Math.floor(soHang /10)
 *
 * Output
 * tong
 *
 */

var soHang = 13;
var soDonvi = soHang % 10;
var soChuc = Math.floor(soHang / 10);

var tong = soDonvi + soChuc;
console.log("tong: ", tong);
